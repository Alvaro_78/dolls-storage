import React, { Component } from 'react'
import data from './firebase.js'
import Form from './components/Form'
import Card from './components/Card'



class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      users: [],
    }

  }
  componentDidMount() {
    this.subcribeUsers()

  }

  async subcribeUsers(user) {
    data.collection('user')
      .onSnapshot((querySnapshot) => {
        const validations = []
        querySnapshot.forEach(doc => {
          validations.push({...doc.data(), id:doc.id})
        })
        this.setState({users: validations})
      })
  }

  handleAdd(user) {
    data.collection('user').add(user)
  }

  render(){
    const users = this.state.users.map((user) => {
      return(
        <Card user={user} key={user.id}/>
        )
      })

      return (

        <div className="App">
            <Form  onAdd={this.handleAdd}/>
            { users }
        </div>
      )
    }
}

export default App