import React, { Component, useState } from 'react'
import {storage} from '../firebase.js'


class Login extends Component {
    constructor() {
        super()
        const [user, setUser] = useState({
            user_email: '',
            user_password: '',
            user_id: ''
          })
    }

      
    updateUserState(event) {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        })
    }

    handleOnKeyDown(event) {
        if(event.keyCode == 13) doesUserExist()
        
    }

    doesUserExist() {
        fetch(`${process.env.API_URL}/check_user_exist`, {
            method: 'POST',
            headers: {'Content-Type' : 'application/json'},
            body: JSON.stringify(user)
        })

        .then(res => res.json()).then (res => {
            if (res.user){
                setUser({
                        user_id: res.user[0]._id,
                        user_password: res.user[0].password,
                        user_email: res.user[0].email
                })
            } else {
                alert(res.error_message)
            }
        })
    }

    render(){
        return (
            <div className="login-container">
                <div className="input-container">
                    <label>Introduce tu e-mail</label>
                    <input
                        name="user_email"
                        className="e-mail" 
                        type="email" 
                        placeholder="tu E-mail@compañia.com"
                        onChange={updateUserState}
                    ></input>
                </div>
                <div className="input-container">
                    <label>Introduce tu contraseña</label>
                    <input
                        name="user_password"
                        className="password"
                        type="password"
                        placeholder="tu contraseña"
                        onChange={updateUserState}
                        onKeyDown={handleOnKeyDown}
                    >
                    </input>

                </div>

                <div className="button-container">
                    <button
                        className="button-login"
                        onClick={doesUserExist}>
                        login
                    </button>
                </div>
            </div>
        )
    }
}

export default Login