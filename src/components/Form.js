import React, {Component} from 'react'
import {storage} from '../firebase.js'
import mypic from '../img/article.jpg'


class Form extends Component {
    constructor(props){
        super(props)
        const user = this.props.user || {}
        this.state = {
            name: user.name || '',
            mold: user.mold || '',
            brand: user.brand || '',
            text_area: user.text_area || '',
            image: user.image || mypic
        }

        this.handleInput = this.handleInput.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.fileSelectedHandler = this.fileSelectedHandler.bind(this)

    }

    handleInput(event){
        const { value, name} = event.target
        this.setState({
            [name]: value
        })
    }

    handleSubmit(event){
      event.preventDefault()
      this.props.onAdd(this.state)
      this.setState({
          name: '',
          mold: '',
          brand: '',
          text_area: '',
          image: mypic
      })
    }

    async fileSelectedHandler(event){
      if(event.target.files[0]){
        const file = event.target.files[0]
        const snapshot = await storage.ref(`images/${file.name}`).put(file)
        const downloadURL = await snapshot.ref.getDownloadURL()
        this.setState({image: downloadURL})
      }
    }

    render(){
        return(
            <div className="card">
              <img src={this.state.image} alt="you"/>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="name">
                            <span>Name:</span>
                            <input
                                type="text"
                                name="name"
                                placeholder="Name"
                                onChange={this.handleInput}
                                value={this.state.name}
                            />
                        </label>
                        <label htmlFor="mold">
                            <span>Mold:</span>
                            <input
                                type="text"
                                name="mold"
                                placeholder="Mold"
                                onChange={this.handleInput}
                                value={this.state.mold}
                            />
                        </label>
                        <label htmlFor="brand">
                            <span>Brand:</span>
                            <input
                                type="text"
                                name="brand"
                                placeholder="Brand"
                                onChange={this.handleInput}
                                value={this.state.brand}
                            />
                        </label>
                        <label htmlFor="text_area">
                            <span>Description:</span>
                            <textarea rows="6" 
                                type="textarea"
                                name="text_area"                        
                                placeholder="Write something about your doll..."
                                onChange={this.handleInput}
                                value={this.state.text_area}
                            />
                            </label>
                    </div>
                    <div className="select">
                        <input type="file" onChange={this.fileSelectedHandler}/>
                           
                    </div>
                    <button type="submit">
                        Add
                    </button>
                </form>
            </div>
        )
    }
}

export default Form