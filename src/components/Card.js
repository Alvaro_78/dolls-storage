import React, { Component } from 'react'
import data from '../firebase.js'
import Form from './Form.js'


class Card extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isEditionMode: false
    }
  }

  render() {
    if (this.state.isEditionMode) {
      return (<Form user = {this.props.user}
        onAdd={this.onEdit.bind(this)}
        />)
      }
      return ( 
        <div className="card">
          <img src={this.props.user.image} alt="you"/ >
            <div className = "user-data" >
              <div>
                <ul> 
                  <li>{this.props.user.name}</li>
                  <li>{this.props.user.mold}</li>
                  <li>{this.props.user.brand}</li>
                </ul >
                <textarea readOnly rows="12"  value={this.props.user.text_area}/>
              </div> 
              <div className="icons">
                <div>
                  <span className ="material-icons" onClick={() => this.handleRemove()}>
                  cancel 
                  </span>
                  delete
                </div>
                <div>
                  <span className="material-icons" onClick={() => this.handleEdit()}>
                  create 
                  </span>
                  edit
                </div> 
              </div>
            </div> 
        </div>
      )
    }

    handleRemove() {
      if (window.confirm('¿Estás seguro de querer borrar usuario?')) {
        data.collection('user').doc(this.props.user.id).delete()
      }
    }

    handleEdit() {
      this.setState({ isEditionMode: true })
    }

    onEdit(user) {
      this.setState({ isEditionMode: false })
      data.collection("user").doc(this.props.user.id).set({
          name: user.name,
          mold: user.mold,
          access: user.access,
          image: user.image
        })
        .then(function() {
          console.log("Document successfully written!")
        })
        .catch(function(error) {
          console.error("Error writing document: ", error)
        })
    }

  }

  export default Card