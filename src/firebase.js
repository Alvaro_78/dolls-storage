import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'

let firebaseConfig = {
   apiKey: "AIzaSyDgd70VY1sGKALJR0r96E9o3bbXqXtxdQY",
   authDomain: "dolls-storage.firebaseapp.com",
   databaseURL: "https://dolls-storage.firebaseio.com",
   projectId: "dolls-storage",
   storageBucket: "dolls-storage.appspot.com",
   messagingSenderId: "830142518474",
   appId: "1:830142518474:web:96cdfa9df4cd98c1bdd033",
   measurementId: "G-9Y68LSM0WQ" }

 const fb = firebase.initializeApp(firebaseConfig)

export const data = fb.firestore()

const storage = fb.storage()

export {
   storage, data as default
}
