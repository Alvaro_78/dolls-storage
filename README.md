This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Introduction

Project is made with React and Firebase, want be a dolls data base, use for free and free code 

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Deployment

- Project is deployed in [Netlify](https://dolls-storage.netlify.app/)
